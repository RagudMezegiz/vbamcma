/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import org.dterhune.vbamcma.vbam.scenario.HomeworldLoader;
import org.dterhune.vbamcma.vbam.scenario.MapType;
import org.dterhune.vbamcma.vbam.scenario.Scenario;
import org.dterhune.vbamcma.vbam.scenario.ScenarioLoader;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

/**
 * Select scenario wizard page controller.
 * @author David Terhune
 */
public class SelectScenarioPageController
{
    /** Scenario combo box. */
    @FXML
    private ComboBox<Scenario> scenario;
    
    /** Number of players combo box. */
    @FXML
    private ComboBox<Integer> players;
    
    /** Map label. */
    @FXML
    private Label mapLabel;
    
    /** Owning wizard page. */
    private SelectScenarioPage owner;
    
    /** Constructor. */
    public SelectScenarioPageController()
    {
        // Initialization will be handled by FXML.
    }
    
    /**
     * Set the owning wizard page.
     * @param owner owning wizard page
     */
    public void setOwner(SelectScenarioPage owner)
    {
        this.owner = owner;
    }
    
    /**
     * Initialize the controller.  This method will be called automatically
     * by the FXML loader.
     */
    @FXML
    public void initialize()
    {
        // Fill combo box with scenarios found by scenario loader
        scenario.getItems().addAll(ScenarioLoader.getScenarios());
    }
    
    /**
     * Handle scenario combo box selection changed event.
     * @param event action event
     */
    @FXML
    private void handleScenarioSelected(ActionEvent event)
    {
        // Tell the owner that a scenario has been selected.
        owner.scenarioSelected();
        
        // Now fill the players combo box with the allowable player counts.
        ObservableList<Integer> items = players.getItems();
        items.clear();
        Scenario scen = scenario.getValue();
        int min = scen.getMinPlayerCount();
        int max = scen.getMaxPlayerCount();
        for (int i = min; i <= max; ++i)
        {
            items.add(i);
        }
        
        if (min == max)
        {
            // Only one player count for this scenario.  Select it automatically.
            players.setValue(min);
            owner.playersSelected(min);
        }
        else
        {        
            // Since no player count has been selected, tell owner that the count is 0.
            owner.playersSelected(0);
        }
    }
    
    /**
     * Handle player count combo box selection changed event.
     * @param event action event
     */
    @FXML
    private void handlePlayerCount(ActionEvent event)
    {
        // Set the Map label
        Scenario scen = scenario.getValue();
        int playerCount = players.getValue();
        MapType map = scen.getMapType();
        mapLabel.setText(map.getLabel(playerCount));
        owner.playersSelected(playerCount);
        
        // And tell the homeworld loader which map we're using.
        if (map == MapType.BY_PLAYER_COUNT)
        {
            // Return the actual map instead of the special code.
            map = MapType.forPlayerCount(playerCount);
        }
        HomeworldLoader.setMap(map);
    }
}
