/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

/**
 * Campaign moderator wizard page controller.
 * @author David Terhune
 */
public class CampaignModeratorPageController
{
    @FXML
    private CheckBox haveCM;
    
    @FXML
    private TextField cmPassword;
    
    /**
     * Returns whether campaign will have a CM.
     * @return have CM flag
     */
    public boolean hasCM()
    {
        return haveCM.isSelected();
    }
    
    /**
     * Returns the CM password.
     * @return CM password
     */
    public String getPassword()
    {
        return cmPassword.getText();
    }

    /**
     * Action event handler for haveCM check box.
     * @param event action event
     */
    @FXML
    private void handleHaveCM(ActionEvent event)
    {
        cmPassword.setEditable(haveCM.isSelected());
    }
}
