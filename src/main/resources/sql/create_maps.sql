-- Copyright (C) 2018 David Terhune.
-- Licensed under the Open Software License version 3.0.
--
-- Make tables for the default maps.

CREATE TABLE HUGE(
    id INTEGER PRIMARY KEY,
    name TEXT,
    importance TEXT,
    size TEXT,
    capacity INTEGER,
    raw INTEGER,
    census INTEGER,
    morale INTEGER,
    productivity INTEGER,
    failed INTEGER,
    empire INTEGER
);

CREATE TABLE LARGE(
    id INTEGER PRIMARY KEY,
    name TEXT,
    importance TEXT,
    size TEXT,
    capacity INTEGER,
    raw INTEGER,
    census INTEGER,
    morale INTEGER,
    productivity INTEGER,
    failed INTEGER,
    empire INTEGER
);

CREATE TABLE MEDIUM(
    id INTEGER PRIMARY KEY,
    name TEXT,
    importance TEXT,
    size TEXT,
    capacity INTEGER,
    raw INTEGER,
    census INTEGER,
    morale INTEGER,
    productivity INTEGER,
    failed INTEGER,
    empire INTEGER
);

CREATE TABLE SMALL(
    id INTEGER PRIMARY KEY,
    name TEXT,
    importance TEXT,
    size TEXT,
    capacity INTEGER,
    raw INTEGER,
    census INTEGER,
    morale INTEGER,
    productivity INTEGER,
    failed INTEGER,
    empire INTEGER
);

CREATE TABLE TINY(
    id INTEGER PRIMARY KEY,
    name TEXT,
    importance TEXT,
    size TEXT,
    capacity INTEGER,
    raw INTEGER,
    census INTEGER,
    morale INTEGER,
    productivity INTEGER,
    failed INTEGER,
    empire INTEGER
);

