/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
/**
 * Database-related classes and interfaces.
 * @author David Terhune
 */
package org.dterhune.vbamcma.vbam.database;
