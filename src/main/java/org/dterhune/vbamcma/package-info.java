/*
 * Copyright (C) 2018 David Terhune.
 * 
 * Licensed under the Open Software License version 3.0.
 */
/**
 * Main package for the VBAM campaign moderator's assistant.
 * @author David Terhune
 */
package org.dterhune.vbamcma;
