/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
/**
 * Campaigns are instances of scenarios.
 * @author David Terhune
 */
package org.dterhune.vbamcma.vbam.campaign;
