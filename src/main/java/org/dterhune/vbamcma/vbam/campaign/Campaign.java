/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.campaign;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.dterhune.vbamcma.ui.ncwiz.PlayerSetupPage;
import org.dterhune.vbamcma.vbam.database.DatabaseUtils;
import org.dterhune.vbamcma.vbam.scenario.Empire;
import org.dterhune.vbamcma.vbam.scenario.Homeworld;
import org.dterhune.vbamcma.vbam.scenario.MapType;
import org.dterhune.vbamcma.vbam.scenario.Scenario;

/**
 * An ongoing campaign.
 * @author David Terhune
 */
public class Campaign
{
    /** Folder in which campaigns will be saved by default. */
    public static final String FOLDER = "Campaigns";
    
    /**
     * Create a new campaign.
     * @param dbFile campaign database file
     * @param settings wizard settings
     * @return new campaign
     */
    public static Campaign create(File dbFile, Map<String, Object> settings)
    {
        if (dbFile.exists())
        {
            boolean deleted = dbFile.delete();
            if (!deleted)
            {
                // TODO Log deletion error
                System.err.printf("Unable to delete database %s; cannot create campaign%n", dbFile);
                return null;
            }
        }
        
        try (Connection conn = DriverManager.getConnection(String.format(DatabaseUtils.CAMPAIGN_CONNECT, dbFile));
            Statement stmt = conn.createStatement())
        {
            // Create all the campaign tables.
            DatabaseUtils.executeSQL("/sql/create_campaign.sql", conn);
            
            // Copy the systems map.
            Scenario scen = (Scenario)settings.get("scenario");
            MapType map = scen.getMapType();
            int players = (Integer)settings.get("players");
            if (map == MapType.BY_PLAYER_COUNT)
            {
                map = MapType.forPlayerCount(players);
            }
            DatabaseUtils.copySystems(map, conn);
            
            // Add universal and raider units.
            DatabaseUtils.copyUniversalUnits(conn);
            
            // Create each empire and add its units
            try (PreparedStatement empireInsert = conn.prepareStatement(DatabaseUtils.EMPIRE_INSERT);
                PreparedStatement homeworldUpdate = conn.prepareStatement(DatabaseUtils.SYSTEMS_UPDATE_EMPIRE))
            {
                for (int i = 1; i <= players; ++i)
                {
                    String playerKey = String.format(PlayerSetupPage.PLAYER_FMT, i);
                    Empire e = (Empire)settings.get(playerKey + PlayerSetupPage.EMPIRE_SUFFIX);
                    Homeworld h = (Homeworld)settings.get(playerKey + PlayerSetupPage.HOMEWORLD_SUFFIX);
                    int ty = (Integer)settings.get(playerKey + PlayerSetupPage.TECHYEAR_SUFFIX);
                    
                    // Insert the empire entry
                    empireInsert.setInt(1, e.id);
                    empireInsert.setString(2, e.name);
                    empireInsert.setInt(3, ty);
                    empireInsert.addBatch();
                    
                    // Update the systems table to own the homeworld
                    homeworldUpdate.setInt(1, e.id);
                    homeworldUpdate.setInt(2, h.id);
                    homeworldUpdate.addBatch();
                    
                    // Copy the empire's units into the campaign database
                    DatabaseUtils.copyEmpireUnits(e.id, conn);
                }
                
                empireInsert.executeBatch();
                homeworldUpdate.executeBatch();
            }
            
            return new Campaign(dbFile);
        }
        catch (SQLException ex)
        {
            // TODO Handle properly
            ex.printStackTrace(System.err);
            return null;
        }
    }
    
    /**
     * Open an existing campaign.
     * @param dbFile campaign database
     * @return campaign, or {@code null} if no such campaign exists
     */
    public static Campaign open(File dbFile)
    {
        if (dbFile.exists())
        {
            return new Campaign(dbFile);
        }
        
        // TODO Log campaign open failure
        return null;
    }
    
    /** Database file. */
    private File dbFile;
    
    /**
     * Constructor.
     * @param dbFile campaign database file
     */
    private Campaign(File dbFile)
    {
        this.dbFile = dbFile;
    }
}
