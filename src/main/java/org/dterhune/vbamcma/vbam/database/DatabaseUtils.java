/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.dterhune.vbamcma.vbam.campaign.Campaign;
import org.dterhune.vbamcma.vbam.scenario.MapType;

/**
 * Constants and utilities for database interaction.
 * @author David Terhune
 */
public class DatabaseUtils
{
    /** Connection string for a new campaign database. */
    public static final String CAMPAIGN_CONNECT = "jdbc:sqlite:%s";
    
    /** Query for all the convoy types. */
    public static final String CONVOY_TYPE_QUERY = "SELECT * FROM CONVOY_TYPES;";
    
    /**
     * Insert a set of values into the EMPIRES table.  The treasury and tech
     * investment values are hard-coded to 0 for the start of a new game.
     */
    public static final String EMPIRE_INSERT =
        "INSERT INTO EMPIRES(id, name, treasury, tech_year, tech_invest) VALUES(?, ?, 0, ?, 0);";
    
    /** Connection string for built-in systems database. */
    public static final String SYSTEMS_CONNECT = "jdbc:sqlite::resource:db/Systems.db";
    
    /** Insert a set of values into the SYSTEMS table. */
    public static final String SYSTEMS_INSERT = "INSERT INTO SYSTEMS (%s) VALUES (%s);";
    
    /** Query of systems from a map type name. */
    public static final String SYSTEMS_MAP_QUERY = "SELECT * FROM %s";
    
    /** Update the empire for a given system. */
    public static final String SYSTEMS_UPDATE_EMPIRE = "UPDATE SYSTEMS SET empire = ? WHERE id = ?;";
    
    /** Connection string for built-in unit types database. */
    public static final String TYPES_CONNECT = "jdbc:sqlite::resource:db/Types.db";
    
    /** Insert a set of values into a TYPES table. */
    public static final String TYPES_INSERT = "INSERT INTO %s_TYPES (%s) VALUES (%s);";
    
    /** Query of all types of one kind belonging to a particular empire. */
    public static final String TYPES_QUERY = "SELECT * FROM %s_TYPES WHERE empire = %d;";
    
    /**
     * Copy the convoy types to the destination database.
     * @param dest destination database
     */
    public static void copyConvoys(Connection dest)
    {
        try (Connection src = DriverManager.getConnection(TYPES_CONNECT);
            Statement s1 = src.createStatement();
            ResultSet rs = s1.executeQuery(CONVOY_TYPE_QUERY))
        {
            ResultSetMetaData meta = rs.getMetaData();
            
            List<String> columns = new ArrayList<>();
            for (int i = 1; i <= meta.getColumnCount(); ++i)
            {
                columns.add(meta.getColumnName(i));
            }
            
            try (PreparedStatement s2 = dest.prepareStatement(String.format(TYPES_INSERT, "CONVOY",
                columns.stream().collect(Collectors.joining(", ")),
                columns.stream().map(c -> "?").collect(Collectors.joining(", ")))))
            {
                while (rs.next())
                {
                    for (int i = 1; i <= meta.getColumnCount(); ++i)
                    {
                        s2.setObject(i, rs.getObject(i));
                    }
                    
                    s2.addBatch();
                }
                
                s2.executeBatch();
            }
        }
        catch (SQLException ex)
        {
            // TODO Handle properly
            ex.printStackTrace(System.err);
        }
    }
    
    /**
     * Copy an empire's units from the default types to the destination database.
     * @param empireID empire ID
     * @param dest destination database
     */
    public static void copyEmpireUnits(int empireID, Connection dest)
    {
        try (Connection src = DriverManager.getConnection(TYPES_CONNECT))
        {
            copyTypes("SHIP", empireID, src, dest);
            copyTypes("FLIGHT", empireID, src, dest);
            copyTypes("BASE", empireID, src, dest);
            copyTypes("GROUND_FORCE", empireID, src, dest);
        }
        catch (SQLException ex)
        {
            // TODO Handle properly
            ex.printStackTrace(System.err);
        }
    }
    
    /**
     * Copy systems from a default map type to the destination database.
     * @param map map type
     * @param dest destination database
     */
    public static void copySystems(MapType map, Connection dest)
    {
        try (Connection src = DriverManager.getConnection(SYSTEMS_CONNECT);
            Statement s1 = src.createStatement();
            ResultSet rs = s1.executeQuery(String.format(SYSTEMS_MAP_QUERY, map)))
        {
            ResultSetMetaData meta = rs.getMetaData();
            
            List<String> columns = new ArrayList<>();
            for (int i = 1; i <= meta.getColumnCount(); ++i)
            {
                columns.add(meta.getColumnName(i));
            }
            
            try (PreparedStatement s2 = dest.prepareStatement(String.format(SYSTEMS_INSERT,
                columns.stream().collect(Collectors.joining(", ")),
                columns.stream().map(c -> "?").collect(Collectors.joining(", ")))))
            {
                while (rs.next())
                {
                    for (int i = 1; i <= meta.getColumnCount(); ++i)
                    {
                        s2.setObject(i, rs.getObject(i));
                    }
                    
                    s2.addBatch();
                }
                
                s2.executeBatch();
            }
        }
        catch (SQLException ex)
        {
            // TODO Handle properly
            ex.printStackTrace(System.err);
        }
    }
    
    /**
     * Copy the specified types of the specified empire from the source
     * database to the destination database.
     * @param type type prefix
     * @param empireID empire ID
     * @param src source database
     * @param dest destination database
     */
    public static void copyTypes(String type, int empireID, Connection src, Connection dest)
    {
        try (Statement s1 = src.createStatement();
            ResultSet rs = s1.executeQuery(String.format(TYPES_QUERY, type, empireID)))
        {
            ResultSetMetaData meta = rs.getMetaData();
            
            List<String> columns = new ArrayList<>();
            for (int i = 1; i <= meta.getColumnCount(); ++i)
            {
                columns.add(meta.getColumnName(i));
            }
            
            try (PreparedStatement s2 = dest.prepareStatement(String.format(TYPES_INSERT, type,
                columns.stream().collect(Collectors.joining(", ")),
                columns.stream().map(c -> "?").collect(Collectors.joining(", ")))))
            {
                while (rs.next())
                {
                    for (int i = 1; i <= meta.getColumnCount(); ++i)
                    {
                        s2.setObject(i, rs.getObject(i));
                    }
                    
                    s2.addBatch();
                }
                
                s2.executeBatch();
            }
        }
        catch (SQLException ex)
        {
            // TODO Handle properly
            ex.printStackTrace(System.err);
        }
    }
    
    /**
     * Copy universal and raider units from the built-in types database to the
     * destination database.
     * @param dest destination database
     */
    public static void copyUniversalUnits(Connection dest)
    {
        // Universal units are those with empire ID of 0.
        copyEmpireUnits(0, dest);
        
        // Raider units are those with empire ID of 99.
        copyEmpireUnits(99, dest);
        
        // Copy the convoys.
        copyConvoys(dest);
    }
    
    /**
     * Executes a SQL script from the classpath.
     * @param scriptFilename script filename
     * @param conn database connection
     * @throws SQLException on database errors
     */
    public static void executeSQL(String scriptFilename, Connection conn) throws SQLException
    {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(
            Campaign.class.getResourceAsStream(scriptFilename)));
            Statement stmt = conn.createStatement())
        {
            StringBuilder sb = new StringBuilder();
            for (String line = in.readLine(); line != null; line = in.readLine())
            {
                // Skip comment lines and blank lines
                String str = line.trim();
                if (str.isEmpty() || str.startsWith("--"))
                {
                    continue;
                }
                
                // Strip trailing comments and append to the current command
                int commentIndex = str.indexOf("--");
                if (commentIndex >= 0)
                {
                    str = str.substring(0, commentIndex).trim();
                }
                sb.append(' ').append(str);
                
                if (str.endsWith(";"))
                {
                    // End of a command.  Execute it.
                    stmt.execute(sb.toString());
                    
                    // And clear the builder for the next command.
                    sb.delete(0, sb.length());
                }
            }
        }
        catch (IOException ex)
        {
            // TODO Handle properly
            ex.printStackTrace(System.err);
        }
    }

    /** Disallow construction. */
    private DatabaseUtils()
    {
        // No initialization needed.
    }
}
