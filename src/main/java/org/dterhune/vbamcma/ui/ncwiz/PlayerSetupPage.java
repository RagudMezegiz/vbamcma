/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.controlsfx.dialog.Wizard;
import org.controlsfx.dialog.WizardPane;
import org.dterhune.vbamcma.vbam.scenario.Empire;
import org.dterhune.vbamcma.vbam.scenario.Homeworld;

import javafx.collections.ObservableMap;
import javafx.fxml.FXMLLoader;

/**
 * Wizard pane for setting up player options.
 * @author David Terhune
 */
public class PlayerSetupPage extends WizardPane
{
    /**
     * Player number format string for properties.
     */
    public static final String PLAYER_FMT = "player%d";

    /**
     * Empire suffix for properties.
     */
    public static final String EMPIRE_SUFFIX = ".empire";

    /**
     * Homeworld suffix for properties.
     */
    public static final String HOMEWORLD_SUFFIX = ".homeworld";
    
    /**
     * Tech year suffix for properties.
     */
    public static final String TECHYEAR_SUFFIX = ".techYear";

    /** Wizard. */
    private Wizard wizard;
    
    /** Page controller. */
    private PlayerSetupPageController controller;
    
    /** Player number. */
    private int playerNumber;
    
    /** Selected empire. */
    private Empire empire = null;
    
    /** Selected homeworld. */
    private Homeworld homeworld = null;
    
    /** Selected tech year. */
    private int techYear;
    
    /**
     * Constructor.
     * @param playerNumber player number
     * @throws IOException if page content cannot be loaded
     */
    public PlayerSetupPage(int playerNumber) throws IOException
    {
        this.playerNumber = playerNumber;
        FXMLLoader fxmlLoader = new FXMLLoader();
        setContent(fxmlLoader.load(getClass().getResourceAsStream("/fxml/ncwiz/playerPage.fxml")));
        controller = fxmlLoader.getController();
        controller.setOwner(this);
    }
    
    /**
     * Returns the player number of this page.
     * @return player number
     */
    public int getPlayerNumber()
    {
        return playerNumber;
    }
    
    /**
     * Notification of an empire being selected.
     * @param empire selected empire
     */
    public void empireSelected(Empire empire)
    {
        this.empire = empire;
        wizard.setInvalid(empire == null || homeworld == null);
    }
    
    /**
     * Notification of a homeworld being selected.
     * @param homeworld selected homeworld
     */
    public void homeworldSelected(Homeworld homeworld)
    {
        this.homeworld = homeworld;
        wizard.setInvalid(empire == null || homeworld == null);
    }
    
    /**
     * Notification of a tech year being selected.
     * @param techYear selected tech year
     */
    public void techYearSelected(int techYear)
    {
        this.techYear = techYear;
    }

    /**
     * Enter page handler.
     * @param wizard hosting wizard
     * @see org.controlsfx.dialog.WizardPane#onEnteringPage(org.controlsfx.dialog.Wizard)
     */
    @Override
    public void onEnteringPage(Wizard wizard)
    {
        super.onEnteringPage(wizard);
        this.wizard = wizard;
        wizard.setInvalid(true);
        
        // Now figure out which empires and homeworlds to remove from the list
        // based on what has been selected already.
        Collection<Empire> empiresToRemove = new ArrayList<>();
        Collection<Homeworld> homeworldsToRemove = new ArrayList<>();
        ObservableMap<String, Object> settings = wizard.getSettings();
        for (int player = 1; player < playerNumber; ++player)
        {
            String keyPrefix = String.format(PLAYER_FMT, player);
            empiresToRemove.add((Empire)settings.get(keyPrefix + EMPIRE_SUFFIX));
            homeworldsToRemove.add((Homeworld)settings.get(keyPrefix + HOMEWORLD_SUFFIX));
        }
        controller.remove(empiresToRemove, homeworldsToRemove);
    }

    /**
     * Exit page handler.
     * @param wizard hosting wizard
     * @see org.controlsfx.dialog.WizardPane#onExitingPage(org.controlsfx.dialog.Wizard)
     */
    @Override
    public void onExitingPage(Wizard wizard)
    {
        // Because each of these pages set the exact same properties, add
        // separate entries for the player number.
        ObservableMap<String, Object> settings = wizard.getSettings();
        String player = String.format(PLAYER_FMT, playerNumber);
        settings.put(player + EMPIRE_SUFFIX, empire);
        settings.put(player + HOMEWORLD_SUFFIX, homeworld);
        settings.put(player + TECHYEAR_SUFFIX, techYear);
        super.onExitingPage(wizard);
    }
}
