-- Copyright (C) 2018 David Terhune.
-- Licensed under the Open Software License version 3.0.
--
-- Create all the necessary tables for a new campaign database.  The tables
-- will be empty and ready to receive data.

-- SYSTEMS table
CREATE TABLE SYSTEMS(
    id INTEGER PRIMARY KEY,
    name TEXT,              -- System name
    importance TEXT,        -- Unimportant, Minor, Major
    size TEXT,              -- Outpost, Settlement, Minor Colony, Colony, Major Colony, Homeworld
    capacity INTEGER,       -- Carrying capacity
    raw INTEGER,            -- Raw materials
    census INTEGER,         -- Census
    morale INTEGER,         -- Morale
    productivity INTEGER,   -- Productivity
    failed INTEGER,         -- Number of failed population increase checks
    empire INTEGER          -- EMPIRES.ID
);

-- EMPIRES table
CREATE TABLE EMPIRES(
	id INTEGER PRIMARY KEY,
	name TEXT,              -- Empire name
	treasury INTEGER,       -- Treasury
	tech_year INTEGER,      -- Tech year
	tech_invest INTEGER     -- Tech investment
);

-- SHIP TYPES table
CREATE TABLE SHIP_TYPES(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	name TEXT,              -- Class name
	isd INTEGER,            -- In service date
	class TEXT,             -- Class type
	cost INTEGER,           -- Cost
	maint_num INTEGER,      -- Maintenance numerator
	maint_den INTEGER,      -- Maintenance denomenator
	dv INTEGER,             -- Defensive Value
	ash INTEGER,            -- Anti-Ship Value
	af INTEGER,             -- Anti-Fighter Value
	cr INTEGER,             -- Command Rating
	cc INTEGER,             -- Command Cost
	cv INTEGER,             -- Carrier Value
	notes TEXT              -- Notes
);

-- FLIGHT TYPES table
CREATE TABLE FLIGHT_TYPES(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	name TEXT,              -- Class name
	isd INTEGER,            -- In service date
	class TEXT,             -- Class type
	cost_num INTEGER,       -- Cost numerator
	cost_den INTEGER,       -- Cost denomenator
	maint_num INTEGER,      -- Maintenance numerator
	maint_den INTEGER,      -- Maintenance denomenator
	dv INTEGER,             -- Defensive Value
	ash INTEGER,            -- Anti-Ship Value
	af INTEGER,             -- Anti-Fighter Value
	notes TEXT              -- Notes
);

-- BASE TYPES table
CREATE TABLE BASE_TYPES(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	name TEXT,              -- Class name
	isd INTEGER,            -- In service date
	class TEXT,             -- Class type
	cost_num INTEGER,       -- Cost numerator
	cost_den INTEGER,       -- Cost denomenator
	maint_num INTEGER,      -- Maintenance numerator
	maint_den INTEGER,      -- Maintenance denomenator
	dv INTEGER,             -- Defensive Value
	ash INTEGER,            -- Anti-Ship Value
	af INTEGER,             -- Anti-Fighter Value
	cv INTEGER,             -- Carrier Value
	notes TEXT              -- Notes
);

-- GROUND FORCE TYPES table
CREATE TABLE GROUND_FORCE_TYPES(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	name TEXT,              -- Unit name
	isd INTEGER,            -- In service date
	cost INTEGER,           -- Cost
	maint_num INTEGER,      -- Maintenance numerator
	maint_den INTEGER,      -- Maintenance denomenator
	atr INTEGER,            -- Attrition
	def INTEGER,            -- Defense
	atk INTEGER,            -- Attack
	df TEXT,                -- D-Factor
	notes TEXT              -- Notes
);

-- CONVOY TYPES table
CREATE TABLE CONVOY_TYPES (
	id INTEGER PRIMARY KEY,
	name TEXT,              -- Class name
	cost INTEGER,           -- Cost
	dv INTEGER,             -- Defensive Value
	ash INTEGER,            -- Anti-Ship Value
	af INTEGER,             -- Anti-Fighter Value
	cr INTEGER,             -- Command Rating
	cc INTEGER,             -- Command Cost
	cv INTEGER,             -- Carrier Value
	notes TEXT              -- Notes
);

-- TASK FORCES table
CREATE TABLE TASK_FORCES(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	name TEXT,              -- Task force name
	location INTEGER        -- SYSTEMS.ID
);

-- SHIPS table
CREATE TABLE SHIPS(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	ship_type INTEGER,      -- SHIP_TYPES.ID
	task_force INTEGER,     -- TASK_FORCES.ID
	escorting INTEGER       -- CONVOYS.ID
);

-- FLIGHTS table
CREATE TABLE FLIGHTS(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	flight_type INTEGER,    -- FLIGHT_TYPES.ID
	parent_type INTEGER,    -- 0 = SYSTEM, 1 = SHIP, 2 = BASE
	parent INTEGER          -- XXX.ID
);

-- BASES table
CREATE TABLE BASES(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	base_type INTEGER,      -- BASE_TYPES.ID
	location INTEGER        -- SYSTEMS.ID
);

-- CONVOYS table
CREATE TABLE CONVOYS(
	id INTEGER PRIMARY KEY,
	empire INTEGER,         -- EMPIRES.ID
	task_force INTEGER,     -- TASK_FORCES.ID
	location INTEGER        -- SYSTEMS.ID
);

-- INTEL table
CREATE TABLE INTEL(
	empire INTEGER,   -- EMPIRES.ID
	location INTEGER, -- SYSTEMS.ID
	quantity INTEGER  -- amount of Intel
);
