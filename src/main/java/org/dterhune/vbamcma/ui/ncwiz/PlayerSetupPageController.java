/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import java.util.Collection;

import org.dterhune.vbamcma.vbam.scenario.Empire;
import org.dterhune.vbamcma.vbam.scenario.EmpireLoader;
import org.dterhune.vbamcma.vbam.scenario.Homeworld;
import org.dterhune.vbamcma.vbam.scenario.HomeworldLoader;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

/**
 * Player setup wizard page controller.
 * @author David Terhune
 */
public class PlayerSetupPageController
{
    /** Empire combo box. */
    @FXML
    private ComboBox<Empire> empire;
    
    /** Homeworld combo box. */
    @FXML
    private ComboBox<Homeworld> homeworld;
    
    /** Tech Year combo box. */
    @FXML
    private ComboBox<Integer> techYear;
    
    /** Owning wizard page. */
    private PlayerSetupPage owner;
    
    /** Constructor. */
    public PlayerSetupPageController()
    {
        // Initialization will be handled by FXML.
    }
    
    /**
     * Set the owning wizard page.
     * @param owner owning wizard page
     */
    public void setOwner(PlayerSetupPage owner)
    {
        this.owner = owner;
        owner.techYearSelected(techYear.getValue());
    }
    
    /**
     * Remove the empires and homeworlds in the collections from the corresponding
     * combo boxes.
     * @param empiresToRemove empires to remove from the empires combo box
     * @param homeworldsToRemove homeworlds to remove from the homeworlds combo box
     */
    public void remove(Collection<Empire> empiresToRemove, Collection<Homeworld> homeworldsToRemove)
    {
        empire.getItems().removeAll(empiresToRemove);
        homeworld.getItems().removeAll(homeworldsToRemove);
    }
    
    /**
     * Initialize the controller.  This method will be called automatically
     * by the FXML loader.
     */
    @FXML
    public void initialize()
    {
        // Fill empire combo box with allowable empires.
        empire.getItems().addAll(EmpireLoader.getEmpires());
        
        // Fill homeworld combo box with allowable homeworlds.
        homeworld.getItems().addAll(HomeworldLoader.getHomeworlds());
        
        // Fill tech year combo box with tech years from 3000 to 3024.
        for (int i = 3000; i < 3025; ++i)
        {
            techYear.getItems().add(i);
        }
        // And select the default tech year of 3000 and notify owner.
        techYear.setValue(3000);
    }

    /**
     * Handle empire combo box selection changed event.
     * @param event action event
     */
    @FXML
    private void handleEmpireSelected(ActionEvent event)
    {
        // Tell the owner that an empire has been selected.
        owner.empireSelected(empire.getValue());
    }
    
    /**
     * Handle homeworld combo box selection changed event.
     * @param event action event
     */
    @FXML
    private void handleHomeworldSelected(ActionEvent event)
    {
        // Tell the owner that a homeworld has been selected.
        owner.homeworldSelected(homeworld.getValue());
    }
    
    /**
     * Handle tech year combo box selection changed event.
     * @param event action event
     */
    @FXML
    private void handleTechYearSelected(ActionEvent event)
    {
        // Tell the owner that a new tech year has been selected.
        owner.techYearSelected(techYear.getValue());
    }
}
