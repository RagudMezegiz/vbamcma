/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import java.io.IOException;

import org.controlsfx.dialog.Wizard;

/**
 * Wizard style dialog for creating a new campaign.
 * @author David Terhune
 */
public class NewCampaignWizard extends Wizard
{
    /** Wizard title. */
    private static final String TITLE = "New Campaign Wizard";
    
    /**
     * Constructor.
     * @throws IOException if any of the wizard pages cannot be loaded
     */
    public NewCampaignWizard() throws IOException
    {
        super(null, TITLE);
        setFlow(new NewCampaignFlow());
    }
}
