/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.scenario;

/**
 * Homeworld information necessary to generate a scenario.
 * @author David Terhune
 */
public class Homeworld
{
    /** Database ID of the homeworld. */
    public final int id;
    
    /** Name of the homeworld. */
    public final String name;
    
    /**
     * Constructor.
     * @param id database ID
     * @param name system name
     */
    public Homeworld(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    /**
     * Returns a string representation.
     * @return human-readable string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return name;
    }

    /**
     * Check with another object for equality.
     * @param obj object to check for equality
     * @return {@code true} if objects are equal, {@code false} otherwise
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null || getClass() != obj.getClass())
        {
            return false;
        }
        Homeworld other = (Homeworld)obj;
        return id == other.id;
    }

    /**
     * Returns a hash code for the object.
     * @return hash code
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return id;
    }
}
