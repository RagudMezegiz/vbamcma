/*
 * Copyright (C) 2018 David Terhune.
 * 
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma;

import org.dterhune.vbamcma.vbam.campaign.Campaign;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main application class for the VBAM campaign moderator's assistant.
 * @author David Terhune
 */
public class App extends Application
{
    /**
     * Single instance of the application.  This works only for a standard desktop
     * application, which this is.  It is not a general-purpose solution for all cases.
     */
    private static App instance;
    
    /**
     * Returns the application instance.
     * @return application instance
     */
    public static synchronized App getInstance()
    {
        return instance;
    }
    
    /**
     * Sets the application instance.
     * @param value application instance
     */
    private static synchronized void setInstance(App value)
    {
        instance = value;
    }
    
    /**
     * Application primary stage.
     */
    private Stage stage;
    
    /**
     * Active campaign.  Set to {@code null} if no campaign is active.
     */
    private Campaign campaign = null;
    
    /**
     * Constructor.
     */
    public App()
    {
        // Don't perform initialization here.  Do it in start method instead.
    }
    
    /**
     * Return the application primary stage.
     * @return primary stage
     */
    public Stage getStage()
    {
        return stage;
    }
    
    /**
     * Returns the active campaign.
     * @return active campaign
     */
    public Campaign getCampaign()
    {
        return campaign;
    }
    
    /**
     * Set a new active campaign.
     * @param campaign active campaign
     */
    public void setCampaign(Campaign campaign)
    {
        this.campaign = campaign;
    }
    
    /**
     * Exit the application.
     */
    public void exit()
    {
        Platform.exit();
    }
    
    /**
     * JavaFX entry point.  Sets up and shows the main application window.
     * @param primaryStage the primary stage for the application, onto which
     *        the application scene will be set
     * @throws Exception if errors occur
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        setInstance(this);
        
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/app.fxml"));
        Scene scene = new Scene(root);
        
        stage.setTitle("VBAM CM Assistant");
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * Main program entry point.
     * @param args program arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }
}
