/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import java.io.IOException;

import org.controlsfx.dialog.WizardPane;

import javafx.fxml.FXMLLoader;

/**
 * Wizard page for setting the campaign moderator password or deciding not to
 * have a campaign moderator.
 * @author David Terhune
 */
public class CampaignModeratorPage extends WizardPane
{
    /**
     * Constructor.
     * @throws IOException if page content cannot be loaded
     */
    public CampaignModeratorPage() throws IOException
    {
        setContent(FXMLLoader.load(getClass().getResource("/fxml/ncwiz/cmPage.fxml")));
    }
}
