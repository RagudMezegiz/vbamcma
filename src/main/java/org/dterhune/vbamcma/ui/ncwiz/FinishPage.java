/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import java.io.IOException;

import org.controlsfx.dialog.WizardPane;

import javafx.fxml.FXMLLoader;

/**
 * Wizard page for showing the final text prior to the user hitting the wizard
 * "Finish" button.
 * @author David Terhune
 */
public class FinishPage extends WizardPane
{
    /**
     * Constructor.
     * @throws IOException if page contents cannot be loaded
     */
    public FinishPage() throws IOException
    {
        setContent(FXMLLoader.load(getClass().getResource("/fxml/ncwiz/finish.fxml")));
    }
}
