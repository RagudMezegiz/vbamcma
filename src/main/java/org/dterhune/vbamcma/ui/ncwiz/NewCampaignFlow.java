/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import java.io.IOException;
import java.util.Optional;

import org.controlsfx.dialog.Wizard.Flow;
import org.controlsfx.dialog.WizardPane;

/**
 * Flow for the new campaign wizard.
 * @author David Terhune
 */
public class NewCampaignFlow implements Flow
{
    /** Campaign Moderator page. */
    private CampaignModeratorPage campaignModPage;
    
    /** Select or Create Scenario page. */
    private SelectScenarioPage scenarioPage;
    
    /** Player setup pages. */
    private PlayerSetupPage[] playerPages;
    
    /** Index of the current player setup page. */
    private int currentPlayerSetupPage;
    
    /** Finish or verification page. */
    private FinishPage finishPage;
    
    /**
     * Constructor.
     * @throws IOException if any of the wizard pages cannot be loaded
     */
    public NewCampaignFlow() throws IOException
    {
        campaignModPage = new CampaignModeratorPage();
        scenarioPage = new SelectScenarioPage();
        finishPage = new FinishPage();
    }

    /**
     * Advance to the next pane.
     * @param pane current pane
     * @return next pane
     * @see org.controlsfx.dialog.Wizard.Flow#advance(org.controlsfx.dialog.WizardPane)
     */
    @Override
    public Optional<WizardPane> advance(WizardPane pane)
    {
        if (pane == null)
        {
            return Optional.of(campaignModPage);
        }
        
        if (pane == campaignModPage)
        {
            return Optional.of(scenarioPage);
        }
        
        if (pane == scenarioPage)
        {
            // Create player setup pages.
            try
            {
                playerPages = new PlayerSetupPage[scenarioPage.getNumPlayers()];
                for (int i = 0; i < playerPages.length; ++i)
                {
                    playerPages[i] = new PlayerSetupPage(i+1);
                }
                currentPlayerSetupPage = 0;
                return Optional.of(playerPages[currentPlayerSetupPage]);
            }
            catch (IOException ex)
            {
                // TODO Implement properly
                ex.printStackTrace(System.err);
            }
        }
        
        if (++currentPlayerSetupPage < playerPages.length)
        {
            return Optional.of(playerPages[currentPlayerSetupPage]);
        }
        
        return Optional.of(finishPage);
    }

    /**
     * Check to see if the wizard can advance to the next pane.
     * @param pane current pane
     * @return {@code true} if advance would return a new pane, {@code false} otherwise
     * @see org.controlsfx.dialog.Wizard.Flow#canAdvance(org.controlsfx.dialog.WizardPane)
     */
    @Override
    public boolean canAdvance(WizardPane pane)
    {
        return pane != finishPage;
    }
}
