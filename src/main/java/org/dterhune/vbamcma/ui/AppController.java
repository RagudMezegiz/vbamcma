/*
 * Copyright (C) 2018 David Terhune.
 * 
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui;

import java.io.File;
import java.io.IOException;

import org.dterhune.vbamcma.App;
import org.dterhune.vbamcma.ui.ncwiz.NewCampaignWizard;
import org.dterhune.vbamcma.vbam.campaign.Campaign;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuBar;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * Application UI controller.
 * @author David Terhune
 */
public class AppController
{
    /**
     * Campaign database extension filter for campaign database files.
     */
    private static final ExtensionFilter DB_FILTER = new ExtensionFilter("Campaign Database", "*.db");
    
    /**
     * Campaign folder.
     */
    private static final File CAMPAIGN_FOLDER = new File(Campaign.FOLDER);
    
    /**
     * Application menu bar.
     */
    @FXML
    private MenuBar menuBar;
    
    /**
     * Constructor.
     */
    public AppController()
    {
        // FXML will handle the initialization.
    }

    /**
     * Initialize the controller.
     */
    @FXML
    public void initialize()
    {
        menuBar.setFocusTraversable(true);
    }
    
    /**
     * Handle the Application|Exit menu action.
     * @param event action event
     */
    @FXML
    private void handleAppExit(ActionEvent event)
    {
        App.getInstance().exit();
    }
    
    /**
     * Handle the Campaign|New menu action.
     * @param event action event
     */
    @FXML
    private void handleCampaignNew(ActionEvent event)
    {
        try
        {
            NewCampaignWizard wiz = new NewCampaignWizard();
            wiz.showAndWait().ifPresent(result -> {
                if (result == ButtonType.FINISH)
                {
                    // Create campaign folder if it doesn't exist
                    if (!CAMPAIGN_FOLDER.exists())
                    {
                        CAMPAIGN_FOLDER.mkdirs();
                    }
                    
                    FileChooser chooser = new FileChooser();
                    chooser.setTitle("Save Campaign File");
                    chooser.setInitialDirectory(CAMPAIGN_FOLDER);
                    chooser.getExtensionFilters().add(DB_FILTER);
                    File selectedFile = chooser.showSaveDialog(App.getInstance().getStage());
                    if (selectedFile == null)
                    {
                        // Display error dialog stating no campaign selected and cancel.
                        new Alert(Alert.AlertType.ERROR,
                            "No campaign file selected; campaign will not be created").showAndWait();
                    }
                    else
                    {
                        App.getInstance().setCampaign(Campaign.create(selectedFile, wiz.getSettings()));
                    }
                }
            });
        }
        catch (IOException ex)
        {
            // TODO Properly handle exception
            ex.printStackTrace(System.err);
        }
    }
    
    /**
     * Handle the Campaign|Open menu action.
     * @param event action event
     */
    @FXML
    private void handleCampaignOpen(ActionEvent event)
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open Campaign File");
        chooser.setInitialDirectory(CAMPAIGN_FOLDER);
        chooser.getExtensionFilters().add(DB_FILTER);
        File selectedFile = chooser.showOpenDialog(App.getInstance().getStage());
        if (selectedFile != null)
        {
            App.getInstance().setCampaign(Campaign.open(selectedFile));
        }
    }
    
    /**
     * Handle the Help|About menu action.
     * @param event action event
     */
    @FXML
    private void handleHelpAbout(ActionEvent event)
    {
        Stage aboutBox = new Stage();
        aboutBox.setTitle("About VBAM CMA");
        
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader();
            Parent root = fxmlLoader.load(getClass().getResourceAsStream("/fxml/about.fxml"));
            AboutBoxController controller = fxmlLoader.getController();
            controller.setStage(aboutBox);
    
            Scene scene = new Scene(root);
            aboutBox.setScene(scene);
            aboutBox.sizeToScene();
            aboutBox.showAndWait();
        }
        catch (IOException ex)
        {
            // TODO Properly handle exception
            ex.printStackTrace(System.err);
        }
    }
}
