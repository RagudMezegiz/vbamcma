/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui.ncwiz;

import java.io.IOException;

import org.controlsfx.dialog.Wizard;
import org.controlsfx.dialog.WizardPane;

import javafx.fxml.FXMLLoader;

/**
 * Wizard pane for selecting or creating the scenario.
 * @author David Terhune
 */
public class SelectScenarioPage extends WizardPane
{
    /** Wizard. */
    private Wizard wizard;
    
    /** Flag indicating a scenario has been selected. */
    private boolean isScenarioSelected = false;
    
    /** Player count. */
    private int playerCount;
    
    /**
     * Constructor.
     * @throws IOException if page content cannot be loaded
     */
    public SelectScenarioPage() throws IOException
    {
        FXMLLoader fxmlLoader = new FXMLLoader();
        setContent(fxmlLoader.load(getClass().getResourceAsStream("/fxml/ncwiz/scenPage.fxml")));
        SelectScenarioPageController controller = fxmlLoader.getController();
        controller.setOwner(this);
    }
    
    /**
     * Returns the number of players.
     * @return player count
     */
    public int getNumPlayers()
    {
        return playerCount;
    }
    
    /**
     * Notification of a scenario being selected within the controller.
     */
    public void scenarioSelected()
    {
        // A scenario has been selected.
        isScenarioSelected = true;
        wizard.setInvalid(playerCount == 0);
    }
    
    /**
     * Notification of the number of players being selected.
     * @param count player count
     */
    public void playersSelected(int count)
    {
        playerCount = count;
        wizard.setInvalid(isScenarioSelected && playerCount == 0);
    }

    /**
     * Enter page handler.
     * @param wizard hosting wizard
     * @see org.controlsfx.dialog.WizardPane#onEnteringPage(org.controlsfx.dialog.Wizard)
     */
    @Override
    public void onEnteringPage(Wizard wizard)
    {
        super.onEnteringPage(wizard);
        this.wizard = wizard;
        wizard.setInvalid(true);
    }
}
