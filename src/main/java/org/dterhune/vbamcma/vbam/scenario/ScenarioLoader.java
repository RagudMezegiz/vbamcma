/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.scenario;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * The scenario loader lists all the scenarios available for new campaigns.
 * Scenarios may be built in or user created.
 * @author David Terhune
 */
public class ScenarioLoader
{
    /**
     * Return the available scenarios.
     * @return available scenarios
     */
    public static Collection<Scenario> getScenarios()
    {
        Collection<Scenario> scenarios = new ArrayList<>();
        
        // Get built-in scenarios.
        scenarios.addAll(getBuiltInScenarios());
        
        // Get custom scenarios.
        scenarios.addAll(getCustomScenarios());
        
        return scenarios;
    }
    
    /**
     * Return the available built-in scenarios.
     * @return built-in scenarios
     */
    private static Collection<Scenario> getBuiltInScenarios()
    {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Scenario>>(){}.getType();
        InputStream is = ScenarioLoader.class.getResourceAsStream("/json/scenarios.json");
        InputStreamReader reader = new InputStreamReader(is);
        return gson.fromJson(reader, listType);
    }
    
    /**
     * Return the available custom scenarios.
     * @return custom scenarios
     */
    private static Collection<Scenario> getCustomScenarios()
    {
        // TODO Implement
        return new ArrayList<>();
    }
    
    /**
     * Constructor.  Private to avoid instantiation.
     */
    private ScenarioLoader()
    {
        // No initialization needed.
    }
}
