/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.scenario;

/**
 * VBAM Scenario.
 * @author David Terhune
 */
public class Scenario
{
    /**
     * Scenario name.  This is the same as its filename without the
     * "scn" extension.
     */
    private String name;
    
    /** Minimum player count. */
    private int minPlayers;
    
    /** Maximum player count. */
    private int maxPlayers;
    
    /** Map type. */
    private MapType map;
    
    /**
     * Constructor.
     */
    public Scenario()
    {
        // Initialization provided by ScenarioLoader or ScenarioCreator.
    }
    
    /**
     * Returns the scenario name.
     * @return scenario name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Returns the minimum player count.
     * @return min player count
     */
    public int getMinPlayerCount()
    {
        return minPlayers;
    }
    
    /**
     * Returns the maximum player count.
     * @return max player count
     */
    public int getMaxPlayerCount()
    {
        return maxPlayers;
    }
    
    /**
     * Returns the map type used in the scenario.
     * @return map type
     */
    public MapType getMapType()
    {
        return map;
    }

    /**
     * Returns a string representation.
     * @return string representation
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return name;
    }
}
