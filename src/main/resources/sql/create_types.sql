-- Copyright (C) 2018 David Terhune.
-- Licensed under the Open Software License version 3.0.
--
-- Make tables for the unit types.

CREATE TABLE SHIP_TYPES (
	id INTEGER PRIMARY KEY,
	empire INTEGER,
	name TEXT,
	isd INTEGER,
	class TEXT,
	cost INTEGER,
	maint_num INTEGER,
	maint_den INTEGER,
	dv INTEGER,
	ash INTEGER,
	af INTEGER,
	cr INTEGER,
	cc INTEGER,
	cv INTEGER,
	notes TEXT
);

CREATE TABLE FLIGHT_TYPES (
	id INTEGER PRIMARY KEY,
	empire INTEGER,
	name TEXT,
	isd INTEGER,
	class TEXT,
	cost_num INTEGER,
	cost_den INTEGER,
	maint_num INTEGER,
	maint_den INTEGER,
	dv INTEGER,
	ash INTEGER,
	af INTEGER,
	notes TEXT
);

CREATE TABLE BASE_TYPES (
	id INTEGER PRIMARY KEY,
	empire INTEGER,
	name TEXT,
	isd INTEGER,
	class TEXT,
	cost_num INTEGER,
	cost_den INTEGER,
	maint_num INTEGER,
	maint_den INTEGER,
	dv INTEGER,
	ash INTEGER,
	af INTEGER,
	cv INTEGER,
	notes TEXT
);

CREATE TABLE GROUND_FORCE_TYPES (
	id INTEGER PRIMARY KEY,
	empire INTEGER,
	name TEXT,
	isd INTEGER,
	cost INTEGER,
	maint_num INTEGER,
	maint_den INTEGER,
	atr INTEGER,
	def INTEGER,
	atk INTEGER,
	df TEXT,
	notes TEXT
);

CREATE TABLE CONVOY_TYPES (
	id INTEGER PRIMARY KEY,
	name TEXT,
	cost INTEGER,
	dv INTEGER,
	ash INTEGER,
	af INTEGER,
	cr INTEGER,
	cc INTEGER,
	cv INTEGER,
	notes TEXT
);
