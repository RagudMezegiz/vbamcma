/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.scenario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import org.dterhune.vbamcma.vbam.database.DatabaseUtils;

/**
 * The homeworld loader lists all the homeworlds available on the current map.
 * The map may be built in or user created.
 * @author David Terhune
 */
public class HomeworldLoader
{
    /** SQL statement for selecting homeworlds from a map. */
    private static final String HOMEWORLD_SQL = "SELECT id, name FROM %s WHERE SIZE='Homeworld';";
    
    /** Current map.  Used to get the homeworlds. */
    private static MapType mapType;
    
    /**
     * Return the available homeworlds on the current map.
     * @return homeworlds
     */
    public static Collection<Homeworld> getHomeworlds()
    {
        try (Connection conn = DriverManager.getConnection(DatabaseUtils.SYSTEMS_CONNECT);
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(String.format(HOMEWORLD_SQL, mapType)))
        {
            Collection<Homeworld> homeworlds = new ArrayList<>();
            
            boolean hasNext = result.next();
            while (hasNext)
            {
                homeworlds.add(new Homeworld(result.getInt(1), result.getString(2)));
                hasNext = result.next();
            }
            
            return homeworlds;
        }
        catch (SQLException ex)
        {
            // TODO Handle properly
            ex.printStackTrace(System.err);
            return new ArrayList<>();
        }
    }
    
    /**
     * Set the map type used for homeworld selection.
     * @param map map type
     */
    public static void setMap(MapType map)
    {
        mapType = map;
    }
    
    /** Disable construction. */
    private HomeworldLoader()
    {
        // No initialization needed.
    }
}
