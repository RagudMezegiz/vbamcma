/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.ui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * About box controller.
 * @author David Terhune
 */
public class AboutBoxController
{
    /**
     * Web view holding the about text.
     */
    @FXML
    private WebView aboutView;
    
    /**
     * Hosting stage.
     */
    private Stage stage;
    
    /**
     * Constructor.
     */
    public AboutBoxController()
    {
        // FXML will handle the initialization.
    }
    
    /**
     * Set the stage hosting this controller.
     * @param stage hosting stage
     */
    public void setStage(Stage stage)
    {
        this.stage = stage;
    }
    
    /**
     * Initialization.  Will be automatically called by the FXML loader once
     * all the controls have been initialized.
     */
    @FXML
    private void initialize()
    {
        WebEngine engine = aboutView.getEngine();
        engine.load(getClass().getResource("/html/about.html").toString());
    }

    /**
     * OK button handler.
     * @param event action event
     */
    @FXML
    private void handleOK(ActionEvent event)
    {
        stage.close();
    }
}
