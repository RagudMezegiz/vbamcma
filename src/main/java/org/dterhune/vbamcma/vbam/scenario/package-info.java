/*
 * Copyright (C) 2018 David Terhune.
 * 
 * Licensed under the Open Software License version 3.0.
 */
/**
 * A scenario consists of a map and a selection of empires.  An instance of a
 * scenario matches empires with their homeworlds, and provides enough
 * information to create a campaign.
 * @author David Terhune
 */
package org.dterhune.vbamcma.vbam.scenario;
