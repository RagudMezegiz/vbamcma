/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.scenario;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * The empire loader lists all the empires available for selection during a scenario.
 * Empires may be built in or user created.
 * @author David Terhune
 */
public class EmpireLoader
{
    /**
     * Return the available empires.
     * @return available empires
     */
    public static Collection<Empire> getEmpires()
    {
        Collection<Empire> empires = new ArrayList<>();
        
        // Add the built-in empires.
        empires.addAll(getBuiltInEmpires());
        
        // Add the custom empires.
        empires.addAll(getCustomEmpires());
        
        return empires;
    }
    
    /**
     * Return the available built-in empires.
     * @return built-in empires
     */
    private static Collection<Empire> getBuiltInEmpires()
    {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Empire>>(){}.getType();
        InputStream is = EmpireLoader.class.getResourceAsStream("/json/empires.json");
        InputStreamReader reader = new InputStreamReader(is);
        return gson.fromJson(reader, listType);
    }

    /**
     * Return the available custom empires.
     * @return custom empires
     */
    private static Collection<Empire> getCustomEmpires()
    {
        // TODO Implement
        return new ArrayList<>();
    }
    
    /** Disallow construction. */
    private EmpireLoader()
    {
        // No initialization needed.
    }
}
