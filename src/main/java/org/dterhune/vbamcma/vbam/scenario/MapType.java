/*
 * Copyright (C) 2018 David Terhune.
 *
 * Licensed under the Open Software License version 3.0.
 */
package org.dterhune.vbamcma.vbam.scenario;

/**
 * Map types used in scenarios.
 * @author David Terhune
 */
public enum MapType
{
    /** Default Tiny map used for 2-player games. */
    TINY,
    /** Default Small map used for 3-player games. */
    SMALL,
    /** Default Medium map used for 4-player games. */
    MEDIUM,
    /** Default Large map used for 5-player games. */
    LARGE,
    /** Default Huge map used for 6-player games. */
    HUGE,
    /** Map is determined by scenario player count. */
    BY_PLAYER_COUNT;

    /**
     * Returns the correct map for the player count.
     * @param playerCount player count
     * @return map for the number of players
     */
    public static MapType forPlayerCount(int playerCount)
    {
        if (playerCount < 2 || playerCount > 6)
        {
            throw new IllegalArgumentException(String.format("%d out of range (2-6)", playerCount));
        }
        return values()[playerCount - 2];
    }
    
    /**
     * Return a label for the map type with the given player count.
     * @param playerCount number of players
     * @return map label text
     */
    public String getLabel(int playerCount)
    {
        switch (this)
        {
        case TINY:
            return "Tiny map";
        case SMALL:
            return "Small map";
        case MEDIUM:
            return "Medium map";
        case LARGE:
            return "Large map";
        case HUGE:
            return "Huge map";
        case BY_PLAYER_COUNT:
            if (playerCount < 2 || playerCount > 6)
            {
                throw new IllegalArgumentException(String.format("%d out of range (2-6)", playerCount));
            }
            return values()[playerCount - 2].getLabel(playerCount);
        }
        
        return "null map";
    }
}
